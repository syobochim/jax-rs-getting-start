package sample;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Ignore;
import org.junit.Test;

import javax.ws.rs.core.Application;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author syobochim
 * @since 1.0
 */
public class HelloResourceTest extends JerseyTest{
    @Override
    protected Application configure() {
        return new ResourceConfig(HelloResource.class);
    }

    @Test
    public void testHelloWorld() throws Exception {
        String response = target("hello")
                .queryParam("name", "world")
                .request()
                .get(String.class);
        assertThat(response, is("Hello, world!"));
    }

    @Ignore
    @Test
    public void sleep() throws Exception {
        System.out.println(getBaseUri());
        Thread.sleep(Long.MAX_VALUE);
    }
}
