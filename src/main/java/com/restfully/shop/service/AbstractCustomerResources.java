package com.restfully.shop.service;

import com.restfully.shop.domain.Customer;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author syobochim
 * @since 1.0
 */
public abstract class AbstractCustomerResources {
    private Map<Integer, Customer> customerDB = new ConcurrentHashMap<>();
    private AtomicInteger idCounter = new AtomicInteger();

    @POST
    @Consumes("application/xml")
    public Response createCustomer(InputStream is) {
        Customer customer = readCustomer(is);
        customer.setId(idCounter.incrementAndGet());
        customerDB.put(customer.getId(), customer);
        System.out.println("Create customer " + customer.getId());
        return Response.created(URI.create("/customers/" + customer.getId())).build();
    }

    @GET
    @Path("{id}")
    @Produces("application/xml")
    public StreamingOutput getCustomer(@PathParam("id") int id) {
        final Customer customer = customerDB.get(id);
        if (customer == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return output -> outputCustomer(output, customer);
    }

    @PUT
    @Path("{id}")
    @Consumes("application/xml")
    public void updateCustomer(@PathParam("id") int id, InputStream inputStream) {
        Customer updateCustomer = readCustomer(inputStream);
        Customer currentCustomer = customerDB.get(id);
        if (currentCustomer == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        currentCustomer.setFirstName(updateCustomer.getFirstName());
        currentCustomer.setLastName(updateCustomer.getLastName());
        currentCustomer.setStreet(updateCustomer.getStreet());
        currentCustomer.setState(updateCustomer.getState());
        currentCustomer.setZip(updateCustomer.getZip());
        currentCustomer.setCountry(updateCustomer.getCountry());
    }

    protected abstract void outputCustomer(OutputStream output, Customer customer) throws IOException;
    protected abstract Customer readCustomer(InputStream is) ;
}
