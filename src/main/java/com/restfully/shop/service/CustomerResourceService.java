package com.restfully.shop.service;

import com.restfully.shop.domain.Customer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * @author syobochim
 */
@Path("/customers")
public class CustomerResourceService extends AbstractCustomerResources {

    protected void outputCustomer(OutputStream output, Customer customer) {
        PrintStream writer = new PrintStream(output);
        writer.println("<customer id=\"" + customer.getId() + "\">");
        writer.println("  <first-name>" + customer.getFirstName() + "</first-name>");
        writer.println("  <last-name>" + customer.getLastName() + "</last-name>");
        writer.println("  <street>" + customer.getStreet() + "</street>");
        writer.println("  <city>" + customer.getCity() + "</city>");
        writer.println("  <state>" + customer.getState() + "</state>");
        writer.println("  <zip>" + customer.getZip() + "</zip>");
        writer.println("  <country>" + customer.getCountry() + "</country>");
        writer.println("</customer>");
    }

    protected Customer readCustomer(InputStream is) {
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(is);
            Element root = doc.getDocumentElement();
            Customer cust = new Customer();
            if (root.getAttribute("id") != null && !root.getAttribute("id").trim().isEmpty()) {
                cust.setId(Integer.valueOf(root.getAttribute("id")));
            }
            NodeList nodes = root.getChildNodes();
            for (int i = 0; i < nodes.getLength(); i++) {
                Element element = (Element) nodes.item(i);
                if ("first-name".equals(element.getTagName())) {
                    cust.setFirstName(element.getTextContent());
                } else if ("last-name".equals(element.getTagName())) {
                    cust.setLastName(element.getTextContent());
                } else if ("street".equals(element.getTagName())) {
                    cust.setStreet(element.getTextContent());
                } else if ("city".equals(element.getTagName())) {
                    cust.setCity(element.getTextContent());
                } else if ("zip".equals(element.getTagName())) {
                    cust.setZip(element.getTextContent());
                } else if ("country".equals(element.getTagName())) {
                    cust.setCountry(element.getTextContent());
                }
            }
            return cust;
        } catch (ParserConfigurationException | IOException | SAXException e) {
            throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
        }
    }
}
