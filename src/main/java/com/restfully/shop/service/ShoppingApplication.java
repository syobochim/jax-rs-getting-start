package com.restfully.shop.service;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author syobochim
 * @since 1.0
 */
@ApplicationPath("start")
public class ShoppingApplication extends Application {
}
