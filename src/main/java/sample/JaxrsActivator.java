package sample;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author syobochim
 */
@ApplicationPath("rest")
public class JaxrsActivator extends Application{

}
